var checkObjectCollision = function(rect1, rect2) {
    return !( rect1.x >= rect2.x + rect2.image.width || rect1.x + rect1.image.width <= rect2.x
    || rect1.y >= rect2.y + rect2.image.height || rect1.y + rect1.image.height <= rect2.y );
}


var _checkObjectRiverSideCollision = function(lvl, objLeftPt, objRightPt, callback){

    var leftLocal = lvl.riversideLeft.globalToLocal(objLeftPt.x, objLeftPt.y);
    var rightLocal = lvl.riversideLeft.globalToLocal(objRightPt.x, objRightPt.y);

    if(lvl.riversideLeft.hitTest(leftLocal.x, leftLocal.y) || lvl.riversideRight.hitTest(rightLocal.x, rightLocal.y)){
        callback();
    }
}
var checkObjectRiverSideCollision = function(objLeftPt, objRightPt, callback){
    var objY = objLeftPt.y;

    if(game.levelOdd){
        var oddS = game.levelOdd.riversideLeft.y;
        var oddE = game.levelOdd.riversideLeft.y - game.levelOdd.levelLength;

        if(objY <= oddS && objY >= oddE){
            _checkObjectRiverSideCollision(game.levelOdd, objLeftPt, objRightPt, callback);
        }
    }
    if(game.levelEven){
        var evenS = game.levelEven.riversideLeft.y;
        var evenE = game.levelEven.riversideLeft.y - game.levelEven.levelLength;

        if(objY <= evenS && objY >= evenE) {
            _checkObjectRiverSideCollision(game.levelEven, objLeftPt, objRightPt, callback);
        }
    }
}

var checkJetRiverSideCollisions = function(){
    if(game.jet.destroyed)  return;

    var jetLeftPoint = {x: game.jet.x, y: game.jet.y + game.jet.jet.image.height/2};
    var jetRightPoint = {x: game.jet.x + 30, y: game.jet.y + game.jet.jet.image.height/2};

    var callback = function(){
        game.jet.destroy();
    }

    if(game.levelOdd) _checkObjectRiverSideCollision(game.levelOdd, jetLeftPoint, jetRightPoint, callback);
    if(game.levelEven) _checkObjectRiverSideCollision(game.levelEven, jetLeftPoint, jetRightPoint, callback);
}

