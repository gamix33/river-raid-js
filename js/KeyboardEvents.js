var KEYCODE_LEFT = 37,
    KEYCODE_RIGHT = 39,
    KEYCODE_UP = 38,
    KEYCODE_DOWN = 40,
    SHOOT = 32,
    START_BTN = 83,
    MENU_BTN = 77,
    INSTRUCTION_BTN = 73,
    SOUND_BTN = 79,
    PAUSE_BTN = 80;

var shot = false;
var shotTmt = null;

function keyDown(event) {
    switch(event.keyCode) {
        case KEYCODE_LEFT:
            if(game.playing){
                game.jet.left();
            }
            break;
        case KEYCODE_RIGHT:
            if(game.playing) {
                game.jet.right();
            }
            break;
        case KEYCODE_UP:
            game.jet.speedFast();
            break;
        case KEYCODE_DOWN:
            game.jet.speedSlow();
            break;
        case SHOOT:
            if(game.playing && !shot) {
                shot = true;
                game.jet.shot();
            }

            if(shotTmt == null){
                shotTmt = setTimeout(function(){
                    shot = false;
                    shotTmt = null;
                }, 100);
            }

            break;
        case START_BTN:
            game.start();
            break;
        case MENU_BTN:
            game.stop();
            menu.showMainMenu();
            break;
        case INSTRUCTION_BTN:
            if(!game.playing){
                menu.showInstruction();
            }
            break;
        case SOUND_BTN:
            sound = !sound;
            break;
        case PAUSE_BTN:
            if(game.playing){
                game.stop();
            }else{
                game.start();
            }
            break;
    }
}

function keyUp(event) {

    switch (event.keyCode) {
        case KEYCODE_LEFT:
        case KEYCODE_RIGHT:
            if (game.playing) {
                game.jet.straighten();
            }
            break;
        case KEYCODE_UP:
        case KEYCODE_DOWN:
            if (game.playing) {
                game.jet.speedNormal();
            }
            break;
    }

}
