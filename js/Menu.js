function Menu(menuGfx, instructionGfx) {
    this.menuGfx = menuGfx;
    this.menuGfx.y = 80;
    this.menuGfx.x = 192;

    this.instructionGfx = instructionGfx;
    this.instructionGfx.y = 80;
    this.instructionGfx.x = 183;
}

Menu.prototype.showMainMenu = function(){
    stage.removeChild(this.instructionGfx);
    stage.addChild(this.menuGfx);

    stage.update();
}

Menu.prototype.showInstruction = function(){
    stage.removeChild(this.menuGfx);
    stage.addChild(this.instructionGfx);
    stage.update();
}

Menu.prototype.close = function(){
    stage.removeChild(this.menuGfx);
    stage.removeChild(this.instructionGfx);
    stage.update();
}
