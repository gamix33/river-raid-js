function Level(startPosition, levelLength, even){
    this.startPosition = startPosition;
    this.color = even ? greenLight : greenDark;
    this.levelLength = levelLength;

    this.roadL = gfx_road.clone();
    this.roadR = gfx_road.clone();

    this.riversideCoords = [];
    this.props = [];

    this.bridgeX = 320;
    this.bridgeHeight = gfx_bridge.image.height;

    this.newLevel();
}

Level.prototype.normalizeY = function(y){
    return (y < -this.levelLength) ? -this.levelLength : y;
}

Level.prototype.newLevel = function(){

    var y = this.startPosition;
    var x = this.bridgeX;
    var prevX;

    this.riversideCoords.push({x: x, y: y});
    y -= 200;
    this.riversideCoords.push({x: x, y: y});

    while(y > -this.levelLength){
        prevX = x;
        y = this.normalizeY(y - Math.floor(50 + Math.random() * 100));

        while(x == null || (prevX != null && Math.abs(x - prevX) < 50)){
            x = 20 + Math.floor(Math.random() * 320);
        }
        this.riversideCoords.push({x: x, y: y});

        y = this.normalizeY(y - Math.floor(50 + Math.random() * 400));
        this.riversideCoords.push({x: x, y: y});
    }

    this.levelLength += this.bridgeHeight + 100;

    this.riversideCoords.push({x: this.bridgeX, y: y - 100});
    this.riversideCoords.push({x: this.bridgeX, y: -this.levelLength});

    this.bridge = null;

    this.draw();
}


Level.prototype.draw = function(){
    this.riversideLeft = new Shape();
    this.riversideRight = new Shape();

    this.riversideLeft.graphics.setStrokeStyle(3)
        .beginFill(this.color)
        .moveTo(0, this.startPosition)
        .beginStroke();

    this.riversideRight.graphics.setStrokeStyle(3)
        .beginFill(this.color)
        .moveTo(stageWidth, this.startPosition)
        .beginStroke();

    for(var i = 0; i < this.riversideCoords.length; i++) {
        this.riversideLeft.graphics.lineTo(this.riversideCoords[i].x, this.riversideCoords[i].y);
        this.riversideRight.graphics.lineTo(stageWidth - this.riversideCoords[i].x, this.riversideCoords[i].y);

        if(this.riversideCoords[i].y % 25 == 0){
            var prop = getRandomInt(0,1) == 0 ? gfx_house.clone() : gfx_tree.clone();
            prop.y = this.riversideCoords[i].y;
            prop.x = this.riversideCoords[i].x - getRandomInt(45, this.riversideCoords[i].x);
            if(getRandomInt(0,1) == 0){
                prop.x = stageWidth - prop.x;
            }
            stage.addChild(prop);
            this.props.push(prop);
        }
    }

    this.riversideLeft.graphics.lineTo(0, -this.levelLength).endStroke();
    this.riversideRight.graphics.lineTo(stageWidth, -this.levelLength).endStroke();

    stage.addChildAt(this.riversideLeft, 0);
    stage.addChildAt(this.riversideRight, 1);

    this.roadL.x = 0;
    this.roadL.y = this.riversideCoords[0].y - gfx_bridge.image.height;
    stage.addChildAt(this.roadL, 2);

    this.roadR.x = stageWidth - 320;
    this.roadR.y = this.riversideCoords[0].y - gfx_bridge.image.height;
    stage.addChildAt(this.roadR, 3);

    this.bridge = new Bridge(this.riversideCoords[0].y - gfx_bridge.image.height);
    this.bridge.draw();

    var y = -70;
    this.opponents = [];
    for(var i = 0; i < (this.levelLength/100)-2; i++){
        y -= getRandomInt(20, 140);
        if(y < -this.levelLength)  break;
        var opponent = new Opponent(y, getRandomInt(2.2, 4.2));
        opponent.draw();
        this.opponents.push(opponent);
    }
    y = getRandomInt(-150, -400);
    this.fuels = [];
    while(y > -(this.levelLength+120)){
        var fuel = new Fuel(y);
        fuel.draw();
        this.fuels.push(fuel);

        y -= getRandomInt(200, 500);
    }
}

Level.prototype.slideDown = function(val){
    this.roadL.y = this.roadR.y += val;
    this.bridge.slideDown(val);
    this.riversideLeft.y = this.riversideRight.y += val;

    for(var i = 0; i<this.props.length; i++){
        this.props[i].y += val;
    }

    return this.riversideLeft.y - this.levelLength;
}


Level.prototype.remove = function(){
    stage.removeChild(this.riversideLeft);
    stage.removeChild(this.riversideRight);
    stage.removeChild(this.roadL);
    stage.removeChild(this.roadR);
    this.bridge.destroy();

    for(var i = 0; i < this.opponents.length; i++){
        this.opponents[i].destroy();
    }

    for(var i = 0; i < this.fuels.length; i++){
        this.fuels[i].destroy();
    }

    for(var i = 0; i < this.props.length; i++){
        stage.removeChild(this.props[i]);
    }

    delete this;
}