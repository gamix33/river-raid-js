function Bullet(){

    this.width = 4;
    this.height = 10;
    this.speed = 5;

    this.bullet = new Shape();
    this.bullet.graphics.beginFill("#ffed14");
    this.bullet.graphics.drawRect(0, 0, this.width, this.height);
    this.bullet.graphics.endFill();

    stage.addChild(this.bullet);

    this.bullet.x = game.jet.x + game.jet.jetWidth / 2 - 2;
    this.bullet.y = game.jet.y - 9;

    if(sound)  SoundJS.play("shot");

    this.ticker = {};
    var self = this;
    Ticker.addListener(self.ticker, false);
    self.ticker.tick = function(){
        self.updatePosition();
    }
}

Bullet.prototype.updatePosition = function(){
    if(!game.playing)  return;
    this.bullet.y = this.bullet.y - this.speed;

    if(this.bullet.y < -10){
        this.destroy();
        return;
    }

    stage.update();
   /* for(var i = 0; i < game.obstacles.length; i++){
        if(game.obstacles[i].y < -10) return;
        if(game.obstacles[i].y > stageHeight){
            game.removeObstacle(i);
            return;
        }

        var self = this;
        this.checkCollision(game.obstacles[i], function(){

            var explosion = gfx_explosion.clone();
            explosion.x = self.bullet.x - 20;
            explosion.y = game.obstacles[i].y + game.obstacles[i].image.height / 2 - 20;
            stage.addChild(explosion);
            stage.update();

            setTimeout(function(){
                stage.removeChild(explosion);
                stage.update();
            }, 100);

            self.destroy();
            game.removeObstacle(i);
        });
    }*/
}

Bullet.prototype.destroy = function(){
    if(this.destroyed)  return;

    this.remove();
    Ticker.removeListener(this.ticker);

    this.destroyed = true;
    delete this;
}

Bullet.prototype.remove = function(){
    if(this.destroyed)  return;
    this.bullet.x = -100;
    stage.removeChild(this.bullet);
    stage.update();
}

Bullet.prototype.checkCollision = function(obj, callback) {
    /*return !(
        ((bullet.y + 7) < (obj2.y)) ||
        (bullet.y > (obj2.y + obj2.image.height)) ||
        ((bullet.x + 4) < obj2.x) ||
        (bullet.x > (obj2.x + obj2.image.width))
    );*/

    /*if(bullet == undefined || obj == undefined){
        return false;
    }*/

    var local = obj.globalToLocal(this.bullet.x, this.bullet.y);

    var hit = obj.hitTest(local.x, local.y);
      /*   (bullet.x+this.width/2 >= obj.x) &&
         (bullet.x+this.width/2 <= obj.x + obj.image.width) &&
         (bullet.y >= obj.y) &&
         (bullet.y <= obj.y + obj.image.height);
*/
     if(hit){
         callback();
     }

     return hit;
    /*

    var local = obj.globalToLocal(bullet.x+this.width/2, bullet.y+this.height/2);
    var hit = obj.hitTest(local.x, local.y);

    if(hit){
        if(typeof callback == "function")  callback();
    }

    return hit;*/
}
