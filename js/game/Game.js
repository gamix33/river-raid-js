function Game(jet, bottomBar){
    this.jet = jet;
    this.bottomBar = bottomBar;

    this.score = 0;
    this.lives = 0;
    this.fuelLevel = 100.0;

    this.playing = false;
    this.during = false;

    this.levelLength = null;
    this.levelOdd = null;
    this.levelEven = null;

    this.obstacles = [];
    this.fuels = [];

    this.mainTicker = {};

    this.lowFuelText = new Text("NISKI POZIOM PALIWA", "20px Arial", "#ff0000");
    this.lowFuelText.x = 290;
    this.lowFuelText.y = 40;
}

Game.prototype.setLives = function(lives){
    this.lives = lives;
    this.bottomBar.updateLives(this.lives);
}

Game.prototype.setScore = function(score){
    this.score = score;
    this.bottomBar.updateScore(this.score);
}

Game.prototype.increaseScore = function(val){
    this.score += val;
    this.bottomBar.updateScore(this.score);
}

Game.prototype.decreaseScore = function(val){
    this.score -= val;
    this.bottomBar.updateScore(this.score);
}

Game.prototype.setFuelLevel = function(fuelLevel){
    this.fuelLevel = fuelLevel;
    this.bottomBar.updateFuelLevel(this.fuelLevel);
}

Game.prototype.startNew = function(){
    if(this.levelOdd){
        this.levelOdd.remove();
        this.levelOdd = null;
    }
    if(this.levelEven){
        this.levelEven.remove();
        this.levelEven = null;
    }

    for(var i = 0; i < this.obstacles.length; i++){
        stage.removeChild(this.obstacles[i]);
    }
    this.obstacles = [];
    for(var i = 0; i < this.fuels.length; i++){
        if(this.fuels[i] && this.fuels[i].destroy) this.fuels[i].destroy();
    }
    this.fuels = [];
    this.bullets = [];

    this.levelLength = 400;
    this.levelOdd = new Level(stageHeight, this.levelLength, true);
    this.setFuelLevel(100.0);

    this.bottomBar.show();
    this.jet.resetPosition();

    this.increaseScore(0);
}

Game.prototype.start = function(){
    menu.close();

    this.removeRespawnText();

    if(!this.during || this.jet.destroyed){
        this.startNew();
    }

    for(var i = 0; i < this.bullets.length; i++){
        this.bullets[i].x = -100;
        stage.removeChild(this.bullets[i]);
    }
    this.bullets = [];

    if(this.lives < 1){
        this.setScore(0);
        this.setLives(3);
    }else{
        this.setLives(this.lives - 1);
    }

    this.playing = true;
    this.during = true;

    var self = this;

    /* Ticker */
    Ticker.setFPS(40);
    Ticker.addListener(self.mainTicker, false);
    self.mainTicker.tick = function(){
        self.update();
    }
}

Game.prototype.stop = function(){
    this.playing = false;
}

Game.prototype.pause = function(){
    if(this.playing){
        this.stop();
    }else{
        this.resume();
    }
}

Game.prototype.showRespawnText = function(){
    var text = this.lives > 0 ? "Naciśnij \"S\" aby wznowić" : "Naciśnij \"S\" aby rozpocząć nową grę";
    this.respawnText = new Text(text, "20px Arial", "#fff");
    this.respawnText.x = this.lives > 0 ? 290 : 240;
    this.respawnText.y = 200;

    stage.addChild(this.respawnText);
    stage.update();
}

Game.prototype.removeRespawnText = function(){
    stage.removeChild(this.respawnText);
    stage.update();
}

Game.prototype.addObstacle = function(obstacle){
    this.obstacles.push(obstacle);
}


Game.prototype.addFuel = function(fuel){
    this.fuels.push(fuel);
}

Game.prototype.removeObstacle = function(i){
    this.obstacles[i].x = -100;
    stage.removeChild(this.obstacles[i]);
    this.obstacles.splice(i, 1);
}

Game.prototype.update = function(){
    if(!this.playing) return;

    this.jet.updatePosition();

    if(this.levelOdd){
        var levelTopY = this.levelOdd.slideDown(this.jet.speedY);

        if(levelTopY >= 0 && this.levelEven == null){
            this.levelLength += 500;
            this.levelEven = new Level(levelTopY - this.jet.speedY, this.levelLength, false);
        }

        if(levelTopY > stageHeight){
            this.levelOdd.remove();
            this.levelOdd = null;
        }
    }
    if(this.levelEven){
        levelTopY = this.levelEven.slideDown(this.jet.speedY);

        if(levelTopY >= 0 && this.levelOdd == null){
            this.levelLength += 500;
            this.levelOdd = new Level(levelTopY, this.levelLength, true);
        }

        if(levelTopY > stageHeight){
            this.levelEven.remove();
            this.levelEven = null;
        }
    }

    if(this.fuelLevel > 0 && !this.jet.destroyed){
        this.fuelLevel -= 0.15;
        this.bottomBar.updateFuelLevel(this.fuelLevel);

        if(this.fuelLevel < 30){
            stage.addChild(this.lowFuelText);
        }else{
            stage.removeChild(this.lowFuelText);
        }
    }else{
        this.jet.destroy();
    }

    checkJetRiverSideCollisions();

    this.bottomBar.show();

    stage.update();
}