function BottomBar(barGfx, width, height){
    this.lives = 0;
    this.score = 0;
    this.fuelLevel = 0.0;

    this.barWidth = width;
    this.barHeight = height;

    this.barGfx = barGfx;
    this.barGfx.x = (stageWidth - this.barWidth) / 2;
    this.barGfx.y = stageHeight - this.barHeight;
}

BottomBar.prototype.updateLives = function(lives){
    stage.removeChild(this.livesText);
    this.lives = lives;

    this.livesText = new Text("Życia: " + this.lives, "20px Arial", "#000");
    this.livesText.x = this.barGfx.x + this.barWidth - 100;
    this.livesText.y = this.barGfx.y + 36;

    stage.addChild(this.livesText);
    stage.update();
}

BottomBar.prototype.updateScore = function(score){
    stage.removeChild(this.scoreText);
    this.score = score;

    this.scoreText = new Text(this.score + " PKT", "20px Arial", "#000");
    this.scoreText.x = this.barGfx.x + this.barWidth - 100;
    this.scoreText.y = this.barGfx.y + 56;

    stage.addChild(this.scoreText);

    stage.update();
}

BottomBar.prototype.updateFuelLevel = function(fuelLevel){
    this.fuelLevel = fuelLevel;
    if(this.fuelPointer)  stage.removeChild(this.fuelPointer);

    this.fuelPointer = new Shape();
    this.fuelPointer.graphics.beginFill("#000");
    this.fuelPointer.graphics.drawRect(0, 0, 4, 35);
    this.fuelPointer.graphics.endFill();

    stage.addChildAt(this.fuelPointer, 999);
    this.fuelPointer.x = 325 + (this.fuelLevel / 100) * 145;
    this.fuelPointer.y = this.barGfx.y + 20;

    stage.update();
}

BottomBar.prototype.update = function(lives, score, fuelLevel){
    this.updateLives(lives);
    this.updateScore(score);
    this.updateFuelLevel(fuelLevel);
}

BottomBar.prototype.show = function(){
    stage.removeChild(this.barGfx);
    stage.addChildAt(this.barGfx, 998);
    this.update(this.lives, this.score, this.fuelLevel);
    stage.update();
}
