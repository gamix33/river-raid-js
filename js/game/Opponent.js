function Opponent(y, speed){

    this.type = getRandomInt(0,1);
    switch(this.type){
        case 0:
            this.leftGfx = gfx_heli1.clone();
            this.rightGfx = gfx_heli1.clone();
            this.rightGfx.setTransform(0,0,-1,1,0,0,0,gfx_heli1.image.width);
            this.gfxType = 1;
            break;
        case 1:
            this.leftGfx = gfx_ship.clone();
            this.rightGfx = gfx_ship.clone();
            this.rightGfx.setTransform(0,0,-1,1,0,0,0,gfx_ship.image.width);
            break;
    };

    this.directionX = getRandomInt(0, 1); //1 - right / 0 - left
    this.gfx = this.directionX ? this.rightGfx : this.leftGfx;

    this.y = this.gfx.y = y;
    this.x = -1;

    this.speedX = speed;

    this.ticker = {};
    var self = this;
    Ticker.addListener(self.ticker, false);
    self.ticker.tick = function(){
        self.updatePosition();
    }

    this.destroyed = false;
}

Opponent.prototype.draw = function(){
    if(this.destroyed)  return;

    stage.addChildAt(this.gfx, 0);
    stage.update();
}


Opponent.prototype.updatePosition = function(){
    if(this.destroyed || !game.playing)  return;

    this.y = this.gfx.y = this.gfx.y + game.jet.speedY;

    if(this.y > 400){
        this.destroy();
        return;
    }

    if(this.y < -15){
        return;
    }else if(this.x == -1){
        function _randomizeX(opponent, call){
            if(call < 5){
                opponent.x = opponent.gfx.x = getRandomInt(50,  stageWidth-50);
                var leftPt = {x: opponent.x, y: opponent.y};
                var rightPt = {x: opponent.x + opponent.gfx.image.width, y: opponent.y};

                call++;
                checkObjectRiverSideCollision(leftPt, rightPt, function(){
                    _randomizeX(opponent, call);
                });
            }else{
                opponent.x = opponent.gfx.x = getRandomInt(380,  420);
            }
        }
        _randomizeX(this, 0);
        game.addObstacle(this.gfx);
    }else{
        this.x = this.gfx.x = this.gfx.x + (this.directionX ? 1 : -1) * this.speedX;
    }

    var self = this;

    var leftPt = {x: this.x - 4, y: this.y};
    var rightPt = {x: this.x + this.gfx.image.width + 4, y: this.y};
    checkObjectRiverSideCollision(leftPt, rightPt, function(){
        self.turnAround();
    });

    this.checkBulletCollsion();
    this.checkJetCollision();

    stage.update();
}

Opponent.prototype.turnAround = function(){
    if(this.destroyed)  return;

    this.remove();
    if(this.directionX){
        this.gfx = this.leftGfx;
        this.directionX = 0;
        this.x -= 10;
    }else{
        this.gfx = this.rightGfx;
        this.directionX = 1;
        this.x += 10;
    }
    this.gfx.x = this.x;
    this.gfx.y = this.y;
    this.draw();
}


Opponent.prototype.destroy = function(){
    if(this.destroyed)  return;

    this.remove();
    Ticker.removeListener(this.ticker);

    this.destroyed = true;
    delete this;
}

Opponent.prototype.remove = function(){
    if(this.destroyed)  return;

    if(stage.contains(this.gfx)){
        stage.removeChild(this.gfx);
        stage.update();
    }
}

Opponent.prototype.checkBulletCollsion = function(){
    if(this.destroyed)  return;

    for(var i = 0; i < game.bullets.length; i++){
        var local = this.gfx.globalToLocal(game.bullets[i].bullet.x, game.bullets[i].bullet.y);

        var hit = this.gfx.hitTest(local.x, local.y);
        if(hit){

            var explosion = gfx_explosion.clone();
            explosion.x = game.bullets[i].bullet.x - 20;
            explosion.y = this.gfx.y + this.gfx.image.height / 2 - 20;
            stage.addChild(explosion);
            stage.update();

            setTimeout(function(){
                stage.removeChild(explosion);
                stage.update();
            }, 100);

            this.destroy();
            game.bullets[i].destroy();
            game.bullets.splice(i, 1);

            game.increaseScore(50);

            if(sound)  SoundJS.play("shot");
        }
    }
}

Opponent.prototype.checkJetCollision = function(){
    if(this.destroyed)  return;

    if (checkObjectCollision(game.jet.jet, this.gfx)) {
        game.jet.destroy();
        this.destroy();
    }
}
