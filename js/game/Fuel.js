function Fuel(y){

    this.gfx = gfx_fuel.clone();
    this.y = this.gfx.y = y;
    this.x = -1;

    this.ticker = {};
    var self = this;
    Ticker.addListener(self.ticker, false);
    self.ticker.tick = function(){
        self.updatePosition();
    }

    this.destroyed = false;
}

Fuel.prototype.draw = function(){
    if(this.destroyed)  return;

    stage.addChildAt(this.gfx, 0);
    stage.update();
}


Fuel.prototype.updatePosition = function(){
    if(this.destroyed || !game.playing)  return;

    this.y = this.gfx.y = this.gfx.y + game.jet.speedY;

    if(this.y > 400){
        this.destroy();
        return;
    }

    if(this.y < -50){
        return;
    }else if(this.x == -1){
        function _randomizeX(opponent, call){
            if(call < 5){
                opponent.x = opponent.gfx.x = getRandomInt(50,  stageWidth-50);
                var leftPt = {x: opponent.x, y: opponent.y};
                var rightPt = {x: opponent.x + opponent.gfx.image.width, y: opponent.y};

                call++;
                checkObjectRiverSideCollision(leftPt, rightPt, function(){
                    _randomizeX(opponent, call);
                });
            }else{
                opponent.x = opponent.gfx.x = getRandomInt(380,  420);
            }
        }
        _randomizeX(this, 0);

        game.addFuel(this.gfx);
    }

    this.checkJetCollision();
    this.checkBulletCollsion();

    stage.update();
}

Fuel.prototype.checkBulletCollsion = function(){
    if(this.destroyed)  return;

    for(var i = 0; i < game.bullets.length; i++){
        var local = this.gfx.globalToLocal(game.bullets[i].bullet.x, game.bullets[i].bullet.y);

        var hit = this.gfx.hitTest(local.x, local.y);
        if(hit){
            var explosion = gfx_explosion.clone();
            explosion.x = game.bullets[i].bullet.x - 20;
            explosion.y = this.gfx.y + this.gfx.image.height / 2 - 20;
            stage.addChild(explosion);
            stage.update();

            setTimeout(function(){
                stage.removeChild(explosion);
                stage.update();
            }, 100);

            this.destroy();
            game.bullets[i].destroy();
            game.bullets.splice(i, 1);

            game.decreaseScore(20);

            if(sound)  SoundJS.play("shot");
        }
    }
}

Fuel.prototype.checkJetCollision = function(){
    if(this.destroyed)  return;

    if (checkObjectCollision(game.jet.jet, this.gfx)) {
        this.destroy();
        game.jet.refuel();
    }
}


Fuel.prototype.destroy = function(){
    if(this.destroyed)  return;

    this.remove();
    Ticker.removeListener(this.ticker);

    this.destroyed = true;
    delete this;
}

Fuel.prototype.remove = function(){
    if(this.destroyed)  return;

    if(stage.contains(this.gfx)){
        stage.removeChild(this.gfx);
        stage.update();
    }
}
