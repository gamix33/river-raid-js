function Bridge(y){

    this.gfx = gfx_bridge.clone();
    this.gfx.y = y;
    this.gfx.x = 320;

    this.destroyed = false;
    this.added = false;
}

Bridge.prototype.draw = function(){
    if(this.destroyed)  return;

    stage.addChildAt(this.gfx, 4);
    stage.update();
}


Bridge.prototype.slideDown = function(val){
    if(this.destroyed || !game.playing)  return;

    this.y = this.gfx.y = this.gfx.y + val;

    if(this.y > 400){
        this.destroy();
        return;
    }

    if(this.y < -50){
        return;
    }else if(!this.added){
        game.addObstacle(this.gfx);
        this.added = true;
    }

    this.checkBulletCollsion();
    this.checkJetCollision();

    stage.update();
}

Bridge.prototype.destroy = function(){
    if(this.destroyed)  return;

    this.remove();

    this.destroyed = true;
    delete this;
}

Bridge.prototype.remove = function(){
    if(this.destroyed)  return;

    if(stage.contains(this.gfx)){
        stage.removeChild(this.gfx);
        stage.update();
    }
}

Bridge.prototype.checkBulletCollsion = function(){
    if(this.destroyed)  return;

    for(var i = 0; i < game.bullets.length; i++){
        var local = this.gfx.globalToLocal(game.bullets[i].bullet.x, game.bullets[i].bullet.y);

        var hit = this.gfx.hitTest(local.x, local.y);
        if(hit){

            var explosion = gfx_explosion.clone();
            explosion.x = game.bullets[i].bullet.x - 20;
            explosion.y = this.gfx.y + this.gfx.image.height / 2 - 20;
            stage.addChild(explosion);
            stage.update();

            setTimeout(function(){
                stage.removeChild(explosion);
                stage.update();
            }, 100);

            this.destroy();
            game.bullets[i].destroy();
            game.bullets.splice(i, 1);

            game.increaseScore(50);

            if(sound)  SoundJS.play("shot");
        }
    }
}

Bridge.prototype.checkJetCollision = function(){
    if(this.destroyed)  return;

    if (checkObjectCollision(game.jet.jet, this.gfx)) {
        game.jet.destroy();
        this.destroy();
    }
}
