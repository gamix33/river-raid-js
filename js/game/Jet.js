function Jet(jetGfx, jetTurningGfx, width, height){
    this.jetWidth = width;
    this.jetHeight = height;

    this.jet = this.jetDefaultGfx = jetGfx;

    this.jetLeft = jetTurningGfx;

    this.jetRight = jetTurningGfx.clone();
    this.jetRight.setTransform(0,0,-1,1,0,0,0,this.jetWidth);

    this.speedX = 2.0;
    this.speedY = 3.0;

    this.shiftX = 0;
    this.destroyed = false;

    this.bullets = [];
}

Jet.prototype.remove = function(){
    if(this.destroyed)  return;

    if(stage.contains(this.jet)){
        stage.removeChild(this.jet);
        stage.update();
    }
}

Jet.prototype.draw = function(){
    if(this.destroyed)  return;

    stage.addChild(this.jet);
    stage.update();
}

Jet.prototype.resetPosition = function(){
    this.destroyed = false;

    this.straighten();
    this.speedNormal();

    this.jet.x = this.x = stageWidth/2 - this.jetWidth/2;
    this.jet.y = this.y = 350;

    this.draw();
    if(sound)  SoundJS.play("engine_start");
}

Jet.prototype.updatePosition = function(){
    if(this.destroyed)  return;

    if(this.shiftX != 0){
        var newX = this.x += this.shiftX;

        if(newX > 0 && newX < stageWidth - this.jetWidth){
            this.jet.x = this.x += this.shiftX;
            this.jet.y = this.y;

            this.draw();
        }
    }
}

Jet.prototype.changeGfx = function(gfx){
    if(this.destroyed)  return;

    if(this.jet != gfx){
        this.remove();

        this.jet = gfx;
        this.jet.x = this.x;
        this.jet.y = this.y;
    }
}


Jet.prototype.left = function(){
    if(this.destroyed)  return;

    if(this.shiftX != -2) {
        this.changeGfx(this.jetLeft);
        this.shiftX = -this.speedX;
    }
}

Jet.prototype.right = function(){
    if(this.destroyed)  return;

    if(this.shiftX != 2) {
        this.changeGfx(this.jetRight);
        this.shiftX = this.speedX;
    }
}

Jet.prototype.straighten = function(){
    if(this.destroyed)  return;

    if(this.speedX != 0) {
        this.shiftX = 0;
        this.changeGfx(this.jetDefaultGfx);
        this.draw();
    }
}

Jet.prototype.speedFast = function(){
    if(this.destroyed)  return;

    this.speedY = 10;
}

Jet.prototype.speedSlow = function(){
    if(this.destroyed)  return;

    this.speedY = 2;
}

Jet.prototype.speedNormal = function(){
    if(this.destroyed)  return;

    this.speedY = 3;
}

Jet.prototype.shot = function(){
    if(this.destroyed)  return;

    var bullet = new Bullet();
    game.bullets.push(bullet);
}


Jet.prototype.destroy = function() {
    if(this.destroyed)  return;

    var explosion = gfx_explosion.clone();
    explosion.x = this.jet.x;
    explosion.y = this.jet.y;
    stage.addChild(explosion);
    stage.update();
    setTimeout(function(){
        stage.removeChild(explosion);
        stage.update();
    }, 100);

    for(var i = 0; i < this.bullets.length; i++){
        if(this.bullets[i]){
            this.bullets[i].destroy();
        }
    }
    this.bullets = [];

    this.jet.x = -1000;
    this.remove();
    this.speedNormal();
    this.destroyed = true;
    if(sound)  SoundJS.play("boom");

    game.showRespawnText();
}

Jet.prototype.refuel = function(){
    if(sound)  SoundJS.play("refuel");
    game.fuelLevel = 100.0;
    game.bottomBar.updateFuelLevel(game.fuelLevel);
}

