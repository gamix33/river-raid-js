function Main() {
    canvas = document.getElementById("stage");
    stage = new Stage(canvas);

    stage.mouseEventsEnabled = false;

    /*SoundJS.FlashPlugin.BASE_PATH = "assets/";
    if (!SoundJS.checkPlugin(true)) {
        alert("Error!");
        return;
    }*/

    manifest = [
        {src:"gfx/back.png", id:"gfxback"},
        {src:"gfx/bar.png", id:"bar"},
        {src:"gfx/bridge.png", id:"bridge"},
        {src:"gfx/explosion.png", id:"explosion"},
        {src:"gfx/fuel.png", id:"fuel"},
        {src:"gfx/heli1.png", id:"heli1"},
        {src:"gfx/heli2.png", id:"heli2"},
        {src:"gfx/house.png", id:"house"},
        {src:"gfx/instruction.png", id:"instruction"},
        {src:"gfx/jet.png", id:"jet"},
        {src:"gfx/jet_turning.png", id:"jetTurning"},
        {src:"gfx/menu.png", id:"menu"},
        {src:"gfx/plane.png", id:"plane"},
        {src:"gfx/road.png", id:"road"},
        {src:"gfx/ship.png", id:"ship"},
        {src:"gfx/tree.png", id:"tree"},
        {src:"sfx/boom.ogg", id:"boom"},
        {src:"sfx/engine.ogg", id:"engine"},
        {src:"sfx/engine_start.ogg", id:"engine_start"},
        {src:"sfx/refuel.ogg", id:"refuel"},
        {src:"sfx/shot.ogg", id:"shot"}
    ];

    preloader = new PreloadJS();
    preloader.installPlugin(SoundJS);
    /*preloader.onProgress = handleProgress;
    preloader.onComplete = handleComplete;*/
    preloader.onFileLoad = handleFileLoad;
    preloader.loadManifest(manifest);

    this.document.onkeydown = keyDown;
    this.document.onkeyup = keyUp;
}

function handleFileLoad(event) {

    switch(event.type)
    {
        case PreloadJS.IMAGE:
            //image loaded
            var img = new Image();
            img.src = event.src;
            img.onload = handleLoadComplete;
            window["gfx_" + event.id] = new Bitmap(img);
            break;

        case PreloadJS.SOUND:
            //sound loaded
            handleLoadComplete();
            break;
    }
}

function handleLoadComplete(event) {
    totalLoaded++;

    if(manifest.length == totalLoaded) {

        game = new Game(
            new Jet(gfx_jet, gfx_jetTurning, 32, 31),
            new BottomBar(gfx_bar, 700, 75)
        );

        //menu object
        menu = new Menu(gfx_menu, gfx_instruction);
        menu.showMainMenu();
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*
 function handleProgress(event) {
 //use event.loaded to get the percentage of the loading
 }

 function handleComplete(event) {
 //triggered when all loading is complete
 }
 */

